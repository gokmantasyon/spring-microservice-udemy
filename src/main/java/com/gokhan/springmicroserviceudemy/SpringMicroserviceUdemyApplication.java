package com.gokhan.springmicroserviceudemy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMicroserviceUdemyApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringMicroserviceUdemyApplication.class, args);
    }

}
