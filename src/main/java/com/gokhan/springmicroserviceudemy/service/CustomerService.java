package com.gokhan.springmicroserviceudemy.service;

import com.gokhan.springmicroserviceudemy.dto.CustomerDto;

import java.util.UUID;

public interface CustomerService {

    CustomerDto getCustomerById(UUID id);
}
