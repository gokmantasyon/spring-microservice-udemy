package com.gokhan.springmicroserviceudemy.service;

import com.gokhan.springmicroserviceudemy.dto.CustomerDto;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Override
    public CustomerDto getCustomerById(UUID id) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setName("name");
        customerDto.setUuid(id);
        return customerDto;
    }
}
