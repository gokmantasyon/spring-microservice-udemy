package com.gokhan.springmicroserviceudemy.dto;

import java.util.UUID;

import lombok.Data;

@Data
public class CustomerDto {

    UUID uuid;
    String name;
}
